from PIL import Image
import graph as gr

def setPix(x,y):
    gr.penColor(pix[x,y][0], pix[x,y][1], pix[x,y][2]);
    gr.point(x,y);


im = Image.open('render.png')
pix = im.load()
gr.windowSize(im.size[0], im.size[1])
gr.canvasSize(im.size[0], im.size[1])

for i in range(im.size[0]):
   for j in range(im.size[1]):
       setPix(i,j);
    
print ("Completed")

gr.run()
